using Documenter, Housing

PKG_HOME = joinpath(dirname(pathof(Housing)), "..")

using LiterateWeave

EXMPL = joinpath(PKG_HOME, "examples")
TUTORIALS = joinpath(PKG_HOME, "docs", "src", "tutorials")
isdir(TUTORIALS) ? nothing : mkdir(TUTORIALS)

exmpls = Housing.examples

map(exmpls) do exmpl
  literateweave(joinpath(EXMPL, exmpl*".jl"), out_path=TUTORIALS, doctype="github")
end

examples_pairs = [exmpl => "tutorials/" * exmpl*".md" for exmpl in exmpls]

makedocs(;
    modules=[Housing],
    format=Documenter.HTML(),
    pages=[
        "Home" => "index.md";
        examples_pairs
    ],
    repo="https://gitlab.com/greimel/Housing.jl/blob/{commit}{path}#L{line}",
    sitename="Housing.jl",
    authors="Fabian Greimel <fabgrei@gmail.com>",
    assets=String[],
 )

