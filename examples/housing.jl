# # A model with homeowners and renters

using Revise #src
using Test #jl
using Housing, DisposableIncomes
using UnPack: @unpack
using NamedTupleTools: ntfromstruct
using QuantEcon: stationary_distributions

using Plots #md

PKG_HOME = joinpath(dirname(pathof(Housing)), "..") #src
UPDATE_TESTS = false #src

# ## Setting up the state space

# Exogenous states (incomes)

z_grid = log.([0.5; 1.0; 1.5])
z_prob = [0.7 0.15 0.15;
          0.2 0.6 0.2;
          0.15 0.15 0.7]
z_MC = MarkovChain(z_prob, z_grid, :y)

exo = ExogenousStateSpace([z_MC])

# ## Define utility function for the case of two goods (consumption good and housing)

# ## Aggregate state

param_base = (β = 0.9, θ = 0.9, η = 1, δ = 0.1, ξ = 0.8159, γ = 2)

# # Case 1: Forever home owners

w_grid_own = LinRange(0.0, 5.0, 70)
endo_own = EndogenousStateSpace((w=w_grid_own,))

r_own = 0.05
p_own = 0.9
param_own  = (param_base..., h_thres = eps(), tt=NoGovernment(), inc=SimpleGrossIncomes(z_MC))

agg_state_own = (p = p_own, r = r_own)

@testset "housing objective and constraint" begin
  states = (endo_state=(w=0.5, h=0.2), exo_state=(y=0.25, ntfromstruct(agg_state_own)...))
  x = [1.0, 1.0]
  𝔼V1 = x -> 0.5
  𝔼V2 = (x,y) -> 0.5
  @test Housing.constraint_nlopt(x, zeros(2), states, param_own, Owner()) ≈ -0.6072266875221286
  @test Housing.objective_nlopt(x, [], states, 𝔼V1, param_own, Owner()) == -0.55
  #@test Housing.objective_nlopt(x, [], states, 𝔼V2, param_own, Owner(state=WealthHouse())) == -0.55
end

@unpack policies_full, val = solve_bellman(endo_own, exo, agg_state_own, param_own, Owner(), rtol=√eps())

dist = stationary_distribution(endo_own, exo, policies_full.next_state)

# 

plt_w = plot(w_grid_own, policies_full.w_next, title="wealth next period", xlab="wealth") #md
plt_m_zoomed = plot(w_grid_own[1:25], policies_full.m[1:25,:], title="mortgage (zoomed)", xlab="wealth") #md
scatter!(w_grid_own[1:25], policies_full.m[1:25,:], title="mortgage (zoomed)", xlab="wealth", c=[1 2 3], label="") #md
 
plt_m = plot(w_grid_own, policies_full.m, title="mortgage", xlab="wealth") #md


plt_dist = plot(w_grid_own, dist, xlab="wealth", title="stationary distribution") #md

plot(plt_w, plt_dist, plt_m, plt_m_zoomed, legend=false) #md

using DelimitedFiles #jl
if UPDATE_TESTS #src
  writedlm(joinpath(PKG_HOME, "test/matrices", "housing-simple-value.txt"), val) #src
  writedlm(joinpath(PKG_HOME, "test/matrices", "housing-simple-dist.txt"), dist) #src
end #src

@testset "regression test housing-simple" begin #jl
  value_test = readdlm(joinpath(PKG_HOME, "test/matrices", "housing-simple-value.txt")) #jl
  dist_test = readdlm(joinpath(PKG_HOME, "test/matrices", "housing-simple-dist.txt")) #jl
  @test val ≈ value_test #jl
  @test isapprox(dist, dist_test, rtol=eps()^(1/3)) #jl
end #jl

# ## Equilibrium

# First, let's compute excess demand

function excess_demand(r, p)
  agg_state_own = (p = p, r = r)

  @unpack policies_full, val = solve_bellman(endo_own, exo, agg_state_own, param_own, Owner(), rtol=√eps())

  dist = stationary_distribution(endo_own, exo, policies_full.w_next)

  (m = sum(dist .* policies_full.m), h=sum(dist .* policies_full.h))
end

excess_demand(r_own, p_own)

# TODO: Housing supply

# # Case 2: Own big, rent small

# Now we want to give households to choice whether to buy or to rent

r = 0.02
p = 1.7
w_grid = LinRange(0.0, 4.0, 70)
endo = EndogenousStateSpace((w=w_grid,))

param_base = (β = 0.93, θ = 0.9, η = 1, δ = 0.1, ξ = 0.8159, γ = 2)

# ϕ = p/ϱ = δ + r # ... price-rent ratio
param_both = (param_base..., ϕ = param_base.δ + r,
  h_thres = 1.2, tt=NoGovernment(), inc=SimpleGrossIncomes(z_MC))
param = [param_both, param_both]

agg_state = (p=p, r=r)

@unpack val, policies_full, policy_hh = solve_bellman(endo, exo, agg_state, param, OwnOrRent0(), maxiter=300, rtol=√eps())

dist = stationary_distribution(endo, exo, policies_full.next_state)
value = val

if UPDATE_TESTS #src
  writedlm(joinpath(PKG_HOME, "test/matrices", "own-rent-value.txt"), value) #src
  writedlm(joinpath(PKG_HOME, "test/matrices", "own-rent-dist.txt"), dist) #src
end #src

@testset "own-rent" begin #jl
  value_test = readdlm(joinpath(PKG_HOME, "test/matrices", "own-rent-value.txt")) #jl
  dist_test = readdlm(joinpath(PKG_HOME, "test/matrices", "own-rent-dist.txt")) #jl
  @test value ≈ value_test #jl
  #@test dist ≈ dist_test #) < 1e-12 #jl
  @test maximum(abs, dist .- dist_test) < 1e-7 #jl
end #jl

let w_grid = w_grid, dist=dist, pol = policies_full, policy_hh=policy_hh
  @unpack a, s, w_next = pol

  ζ = (a = sum(dist .* a), h=sum(dist .* s))
  @show ζ
  
  owner = policy_hh .== 1

  plt_own = plot(w_grid, owner, title="Who owns?") #md

  plt_h = plot(w_grid, s, legend=:false, title="House size", markerstrokewidth=0, xlab="wealth") #md

  plt_a = plot(w_grid, a, title = "asset/mortgage") #md

  plt_w = plot(w_grid, w_next, title = "wealth next period") #md

  plt_dist = plot(w_grid, dist, title = "stationary distribution") #md

  plot(plt_h, plt_a, plt_w, plt_dist, legend=false) |> display #md
end

#- #md

# # Overlapping generations
w_grid = LinRange(0.0, 7, 40)
endo = EndogenousStateSpace((w = w_grid, ))

function V_term(states)
  @unpack w = states.endo_state
  ν = 100
  lux = 7.7
  
  ν * Housing.u(w + lux, γ=2)
end

chh = OwnOrRent0()
t_grid = 25:65

param_both = (param_base..., ϕ = param_base.δ + agg_state.r, h_thres = 1.2, tt=NoGovernment(), inc=SimpleGrossIncomes(z_MC))
param = [param_both, param_both]

# param_bothT = (β = 0.93, θ = 0.9, δ = 0.1, h_thres = 1.2, tt=NoGovernment(), inc=SimpleLifecycleGrossIncomes(mc_work=z_MC, age_profile=zeros(size(t_grid))))
# paramT = [param_bothT, param_bothT]

exoT = ExogenousStateSpace(MarkovChainT(z_prob, named_grid(z_grid, :y), t_grid))

out = solve_bellman(endo, exoT, agg_state, param, chh, t_grid, V_term)

plot(out.policies_full.h[:,:,1], legend=false) #md
 plot!(out.policies_full.h[:,:,5]) #md
 plot!(out.policies_full.h[:,:,27]) #md
 plot!(out.policies_full.h[:,:,end-5]) #md
 plot!(out.policies_full.h[:,:,end]) #md

#-

plot(w_grid, out.policies_full.w_next[:,:,1], legend=false) #md
 plot!(w_grid, out.policies_full.w_next[:,:,10]) #md
 plot!(w_grid, out.policies_full.w_next[:,:,20]) #md
 plot!(w_grid, out.policies_full.w_next[:,:,30]) #md
 plot!(w_grid, out.policies_full.w_next[:,:,end]) #md
 
# ### Cross-sectional distribution

π₀ = zeros((length(endo), length(exo)))

dist = stationary_distributions(exo.mc)[1]

π₀[2,:] .= dist ./ sum(dist) .* 0.3
π₀[3,:] .= dist ./ sum(dist) .* 0.4
π₀[4,:] .= dist ./ sum(dist) .* 0.3

sum(π₀)

@unpack distribution = cross_sectional_distribution(endo, exoT, t_grid, out.policies_full.next_state, π₀)

if UPDATE_TESTS #src
  writedlm(joinpath(PKG_HOME, "test/matrices", "own-rent-olg-value.txt"), reshape(out.W, :, length(t_grid)+1)) #src
  writedlm(joinpath(PKG_HOME, "test/matrices", "own-rent-olg-dist.txt"), reshape(distribution, :, length(t_grid))) #src
end #src

@testset "own-rent-olg" begin #jl
  value_test = readdlm(joinpath(PKG_HOME, "test/matrices", "own-rent-olg-value.txt")) #jl
  dist_test = readdlm(joinpath(PKG_HOME, "test/matrices", "own-rent-olg-dist.txt")) #jl
  @test reshape(out.W, :, length(t_grid)+1) ≈ value_test #jl
  @test maximum(abs, reshape(distribution, :, length(t_grid)) - dist_test) < 1e-7 #jl
end #jl

using Colors #md

distribution_t_r = reshape(distribution, (length(endo), length(exo), length(t_grid)))
col = [range(colorant"deepskyblue", stop=colorant"navyblue", length=length(t_grid)), #md
       range(colorant"lightsalmon", stop=colorant"red4", length=length(t_grid)), #md
       range(colorant"yellowgreen", stop=colorant"darkgreen", length=length(t_grid))] #md

map(1:3) do i #md
  plt_dist = plot(legend=false) #md
  for j in 1:length(t_grid) #md
    plot!(plt_dist, distribution_t_r[:,i,j], color=col[i][j]) #md
  end #md
  plt_dist #md
end |> x -> plot(x...)  #md

