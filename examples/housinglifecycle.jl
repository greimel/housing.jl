# # Solving a lifecycle model with owners and renters

using Revise #src
using Test #jl
using Housing, DisposableIncomes
using UnPack: @unpack
using Interpolations: BSpline, Linear
using QuantEcon: tauchen, stationary_distributions

PKG_HOME = joinpath(dirname(pathof(Housing.Aiyagari)), "..")

include(joinpath(PKG_HOME, "src/analyze.jl"))
include(joinpath(PKG_HOME, "src/visualize.jl")) #md

# ## Specifying the income process and the exogenous state space

# 1. specify the AR-shock process
n_y = 5
y = tauchen(n_y, 0.91, 0.21)

z_MC = MarkovChain(y.p, y.state_values, :y)

# 2. specify the structure of the income process (includes lifecycle profile)
inc = SimpleLifecycleGrossIncomes(mc_work=z_MC)

T_work = length(inc.age_profile)
T_ret = 25
t_grid = 1:(T_work + T_ret)

# 3. Construct age-dependent transition matrices (no shocks after retirement!)
transition_matrices = [transition_matrix(inc, t) for t in t_grid]
z_named_grid = named_grid(z_MC)

# 4. Simple exogenous state space

exoT1 = ExogenousStateSpace(
          MarkovChainT(
            transition_matrices,
            z_named_grid,
            t_grid
          ))

# 5. Add house prices as another exogenous state

import StateGrids: product_of_trans_matrices, product_of_named_grids

p_grid = [0.7, 0.9, 1.1]
p_prob = [0.99  0.01 0.0;
          0.005 0.99 0.005;
          0.0   0.01 0.99]

p_named_grid = named_grid(p_grid, :p)

combined_matrices = map(transition_matrices) do Q
  product_of_trans_matrices(Q, p_prob)
end

combined_grid = product_of_named_grids(z_named_grid, p_named_grid)

mcT = MarkovChainT(
          combined_matrices,
          combined_grid,
          t_grid
          )

exoT2 = ExogenousStateSpace(mcT, [z_named_grid, p_named_grid]);

# ## Adjustment costs for housing and other parameters

tt = TaxesTransfers(inc)

param_base = (β = 0.9175 * (1 - 1/45), θ = 0.8, η=1, δ = 0.022, ξ=0.8159,
              itp_scheme = BSpline(Linear()),
              tt=tt,
              inc=inc)

par = (ν = 20, lux = 0.5, ρx=1.7, p=2.0, r=0.0245) # hor from 0 to 80

param_ownT  = (param_base..., h_thres = 1.4)
param_rentT = (param_base..., h_thres = 1.4, ϕ = par.ρx * (param_base.δ + par.r))
#100, 7.7

agg_state = (p=par.p, r=par.r)

# # Bequests
function V_term(states)
  @unpack w = states.endo_state
  @unpack ν, lux = par

  ν * Housing.u(w + lux, γ=2)
 end

# # Case 1: No adjustment costs, housing is not a state
noadj = NoAdjustmentCosts()
own_rent00 = OwnOrRent0()

w_grid1 = LinRange(0.1, 40, 150)
endo1 = EndogenousStateSpace((w=w_grid1,))

endo = endo1
hh = own_rent00
paramT = [param_ownT, param_rentT]

@time outT0 = solve_bellman(endo, exoT1, agg_state, paramT, hh, t_grid, V_term);

# ## Cross-sectional distribution
# initial distribution
π₀ = zeros((length(endo), length(exoT1)))

 dist = stationary_distributions(z_MC)[1]

 π₀[2,:] .= dist ./ sum(dist) .* 0.3
 π₀[3,:] .= dist ./ sum(dist) .* 0.4
 π₀[4,:] .= dist ./ sum(dist) .* 0.3
 
 sum(π₀)
 outT0.policies_full.next_state
 
 @unpack distribution = cross_sectional_distribution(endo, exoT1, t_grid, outT0.policies_full.next_state, π₀)
   
 distribution_t_r = reshape(distribution, (length(endo), length(exoT1), length(t_grid)))

 plot( #md
  plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :h), #md
  #plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :c, rent_code=par.renter), #md
  plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :w_next, grp_var=:occupancy), #md
  plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :w_next), #md
  #plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :a, rent_code=par.renter),  #md
  #plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :inc, rent_code=3),  #md
  plot([mean(outT0.policies_full.occupancy[:,:,i][:] .== :owner, Weights(distribution[:,:,i][:])) for i in t_grid], title="homeownership rate"), #md
  #legend=false #md
 ) #md

# # Case 2: Adjustment costs, housing is a state

adj = ProportionalAdjustmentCosts()
#own_rent0 = OwnOrRent(adjust = AdjustHouse(adj = noadj, state = WealthHouse()), renter = Renter(state=WealthHouse(), adj = noadj))
own_rent  = OwnOrRent(adjust = AdjustHouse(adj = adj, state = WealthHouse()), renter = Renter(state=WealthHouse(), adj = adj))

 
w_grid = LinRange(0.1, 30, 40)
h_grid = LinRange(√eps(), 10, 25)
endo2 = EndogenousStateSpace((w=w_grid, h=h_grid))

endo = endo2
hh = own_rent
paramT = [param_ownT, param_ownT, param_rentT]

@time outT0 = solve_bellman(endo, exoT1, agg_state, paramT, hh, t_grid, V_term); 

# ## Cross-sectional distribution
# initial distribution
π₀ = zeros((length(endo), length(exoT1)))

 dist = stationary_distributions(z_MC)[1]

 π₀[2,:] .= dist ./ sum(dist) .* 0.3
 π₀[3,:] .= dist ./ sum(dist) .* 0.4
 π₀[4,:] .= dist ./ sum(dist) .* 0.3
 
 sum(π₀)
 outT0.policies_full.next_state
 
 @unpack distribution = cross_sectional_distribution(endo, exoT1, t_grid, outT0.policies_full.next_state, π₀)
   
 distribution_t_r = reshape(distribution, (length(endo), length(exoT1), length(t_grid)))

 plot( #md
  plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :h), #md
  #plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :c, rent_code=par.renter), #md
  plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :w_next, grp_var=:occupancy), #md
  plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :w_next), #md
  #plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :a, rent_code=par.renter),  #md
  #plt_aging_quantiles(t_grid, outT0.policies_full, distribution_t_r, :inc, rent_code=3),  #md
  plot([mean(outT0.policies_full.occupancy[:,:,i][:] .== :owner, Weights(distribution[:,:,i][:])) for i in t_grid], title="homeownership rate"), #md
  #legend=false #md
 ) #md

