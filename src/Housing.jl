module Housing

using Reexport: @reexport
using Parameters: @unpack
using NLopt: Opt, lower_bounds!, upper_bounds!, xtol_rel!, ftol_rel!, maxeval!, max_objective!, optimize, inequality_constraint!
import ForwardDiff
using DisposableIncomes: disposable_income

@reexport using Aiyagari

include("helpers.jl")
include("common.jl")
include("owner.jl")
include("renter.jl")

include("housing-wealth-effects-utils.jl")

examples = ["housing", "housinglifecycle"]

end # module
