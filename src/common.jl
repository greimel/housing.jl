u(c; γ) = c^(1-γ) / (1-γ)

function u(c,h; ξ=0.8159, ρ=map(s -> (s-1)/s, 0.13), γ=2.0)
  C = (ξ * h^ρ + (1-ξ) * c^ρ)^(1/ρ)
  u(C, γ=γ)
end

wealth(state, params, hh) = wealth(state, params, hh, hh.state)

function wealth(states, params, hh::Union{Owner, Renter}, s::XAssetHouse)
  @unpack endo_state, exo_state = states
  @unpack p, r = exo_state
  @unpack δ, pₘᵢₙ, θ = params
  @unpack w_tilde, h = endo_state

  
  a = w_tilde - h * pₘᵢₙ * θ *(1-δ)/(1+r)
  
  w = (1+r) * a + (1-δ) * p * h
end

function wealth(states, params, hh::Union{Owner,Renter}, s::Union{WealthHouse,OneState})
  @unpack endo_state, exo_state = states
  @unpack w = endo_state

  w  
end
