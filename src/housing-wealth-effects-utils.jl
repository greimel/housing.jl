using Strided
using SparseArrays
using StructArrays

function initial_cross_sectional_distribution(π₀, i_p, transitions, endo, exo, t_grid)
  distribution = zeros(length(π₀), length(t_grid))

  initial_cross_sectional_distribution!(distribution, π₀, i_p, transitions, endo, exo, t_grid)

  distribution
end

function initial_cross_sectional_distribution!(distribution, π₀, i_p, transitions, endo, exo, t_grid)  
  crosssectional_distribution!(distribution, distribution, i_p => i_p, π₀, transitions, endo, exo, t_grid)
end

function move_cross_sectional_distribution_forward(distributionₜ, π₀, i_p::Pair, transitions, endo, exo, t_grid)
  distributionₜ₊₁ = zeros(length(π₀), length(t_grid))
  
  crosssectional_distribution!(distributionₜ₊₁, distributionₜ, i_p, π₀, transitions, endo, exo, t_grid)
  
  distributionₜ₊₁
end

function crosssectional_distribution!(distributionₜ₊₁, distributionₜ, i_p::Pair, π₀, transitions, endo, exo, t_grid)
  distributionₜ₊₁[:,1] .= vec(π₀)
  sum(π₀) ≈ 1 || @show sum(π₀)

  for j in 1:(length(t_grid) - 1)
    πⱼ = permutedims(sview(distributionₜ, :,j))
    πⱼ₊₁ = permutedims(sview(distributionₜ₊₁,:,j+1))
    
    Π = transitions[j]
    Π_r = reshape(Π, length(endo), size(exo)..., length(endo), size(exo)...)
    Π_small = Π_r[:,:,i_p[1],:,:,i_p[2]]
    Π_r_sq = reshape(Π_small, length(π₀), length(π₀))
    πⱼ₊₁ .= πⱼ * (Π_r_sq ./ sum(Π_r_sq, dims=2))
  end
end

pol_price(out, endo, exo, i_p, t_grid) = out.policies_full |> 
          arr -> reshape(arr, length(endo), size(exo)..., length(t_grid)) |>
          arr -> getindex(arr, :,:,i_p,:) |>
          arr -> reshape(arr, :, length(t_grid)) |>
          StructArray

function aggregates(π, pol, p = 1)
  h = sum(π .* pol.h) 
  (c=sum(π .* pol.c),
   h=h,
   ph = h * p,
   a=sum(π .* pol.a),
   w_tilde=sum(π .* pol.w_tilde),
   w=sum(π .* pol.w_next))
end


