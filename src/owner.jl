w_tilde_next(a_next, h_next, states, params, hh) = w_tilde_next(a_next, h_next, states, params, hh, hh.state)

  #a ≧ - p * h * (1-δ) * θ / (1+r)
  # w_tilde = a + p_min * h * (1-δ) * θ / (1+r)
function w_tilde_next(a_next, h_next, states, params, hh::Owner, s::XAssetHouse)
  @unpack p, r = states.exo_state
  @unpack δ, pₘᵢₙ, θ = params
  
  w_tilde_next = a_next + h_next * pₘᵢₙ * θ *(1-δ)/(1+r)
  
end

w_tilde_next(a_next, h_next, states, params, hh::Owner, s) =
  wealth(states, params, hh, s)

function w_next(c, h_next, states, params, hh::Owner)
  @unpack endo_state, exo_state = states
  @unpack p, r = exo_state
  @unpack δ = params
  
  m_next_ = m_next(c, h_next, states, params, hh)
   
  w_next = (1-δ) * p * h_next - (1+r) * m_next_
  #w_next1 = (1+r) * (w + inc - c - χ(endo_state, p, h_next, hh.adj)) - p * h_next * (r + δ) 
  #@assert w_next1 ≈ w_next
  w_next
end

function m_next(c, h_next, states, params, hh::Owner)
  @unpack endo_state, exo_state = states
  @unpack p = exo_state
  
  w = wealth(states, params, hh)
  inc = disposable_income(params.inc, params.tt, exo_state)

  m = p * h_next + c - inc - w + χ(endo_state, h_next, p, hh.adj)
end

function h_max(states, params, hh::Owner)
  @unpack endo_state, exo_state = states
  @unpack p, r = exo_state
  @unpack δ, θ = params

  w = wealth(states, params, hh)  
  inc = disposable_income(params.inc, params.tt, exo_state)

  # TODO: this is not very general, adj costs depend on choice of h
  h_max = (w + inc - χ(endo_state, 0.0, p, hh.adj)) / (p * (1 - (1-δ) * θ / (1+r)))
end

function c_max(h_next, states, params, hh::Owner)
  @unpack endo_state, exo_state = states
  @unpack p, r = exo_state
  @unpack δ, θ = params
  
  w = wealth(states, params, hh)
  inc = disposable_income(params.inc, params.tt, exo_state)
  
  c_max = w + inc - p * h_next * (1 - θ * (1-δ)/(1+r)) - χ(endo_state, h_next, p, hh.adj)
end

objective0(c, h_next, states, 𝔼V, params, hh) = objective0(c, h_next, states, 𝔼V, params, hh, hh.state)

function objective0(c, h_next, states, 𝔼V, params, hh::Owner, s::OneState)
  @unpack β, η, ξ = params
  
  w_next_ = w_next(c, h_next, states, params, hh)

  u(c, η * h_next, ξ=ξ) + β * 𝔼V(w_next_)    
end

function objective0(c, h_next, states, 𝔼V, params, hh::Owner, s::WealthHouse)
  @unpack β, η, ξ = params
  
  w_next_ = w_next(c, h_next, states, params, hh)

  u(c, η * h_next, ξ=ξ) + β * 𝔼V(w_next_, h_next)
end

function objective0(c, h_next, states, 𝔼V, params, hh::Owner, s::XAssetHouse)
  @unpack β, η, ξ = params
  
  a_next_ = - m_next(c, h_next, states, params, hh)

  w_tilde_next_ = w_tilde_next(a_next_, h_next, states, params, hh)
  
  u(c, η * h_next, ξ=ξ) + β * 𝔼V(w_tilde_next_, h_next)
end

# function objective0(c, h, states, 𝔼V, params, hh::Owner{<:Aiyagari.Conditional})
#   @unpack β = params
# 
#   w_next_ = w_next(c, h, states, 𝔼V, params, hh)
# 
#   u(c,h) + β * 𝔼V([w_next_, w_next_, w_next_ - 0.8 * agg_state.p * h])    
# end

function constraint0(c, h, states, params, hh::Owner)
  @unpack p, r = states.exo_state
  @unpack β, θ, δ = params
  
  m = m_next(c, h, states, params, hh::Owner)
  
  #(1+r) * m <= p * h * (1-δ) * θ
  (1+r) * m - (p * h * (1-δ) * θ)
end

next_state(w_tilde, w, h, ::XAssetHouse) = (w_tilde, h)
next_state(w_tilde, w, h, ::WealthHouse) = (w, h)
next_state(w_tilde, w, h, ::OneState) = w
  
function Aiyagari.get_optimum(states, 𝔼V, params, a_grid, hh::Owner)
  @unpack h_thres = params
  
  inc = disposable_income(params.inc, params.tt, states.exo_state)

  # 1. check if feasible set is non-empty
  h_max_ = h_max(states, params, hh)
  # h_max_ <= 0 && @warn "h_max < 0!!"
  
  if h_max_ < h_thres # can only happen if h_thres > 0, i.e. if renting is available
    conv = true
    c_min = h_min = a_min = √eps()
    
    val = - sqrt(sqrt(prevfloat(Inf)))
#    val = -1e4 * abs(u(c_min, h_min)) + 1e4 * (h_max_ - h_thres)
    
    pol_full = (c=c_min, s=h_min, h=h_min, m=NaN, a=NaN, w_next=NaN, w_tilde=NaN, inc=inc, occupancy=:owner, p=states.exo_state.p, ret=:infeasible, conv=conv, count=0)  
  else
    opt = Opt(:LD_MMA, 2)
    #opt = Opt(:LD_SLSQP, 2)
    lower_bounds!(opt, [eps(), params.h_thres])
    
    xtol_rel!(opt, 1e-10)
    ftol_rel!(opt, 1e-10)
    
    maxeval!(opt, 250)
    
    max_objective!(opt, (x,g) -> objective_nlopt(x, g, states, 𝔼V, params, hh))
    inequality_constraint!(opt, (x,g) -> constraint_nlopt(x, g, states, params, hh), 1e-12)
    
    guess_h = max(h_thres + eps(), h_max_ * 0.05)
    guess_c = c_max(guess_h, states, params, hh) * 0.05

    guesses = [guess_c, guess_h]
    (max_f, max_x, ret) = optimize(opt, guesses)

    val = max_f
    c, h = max_x
    m = m_next(c, h, states, params, hh)
    w_ = w_next(c, h, states, params, hh)
    w_tilde_next_ = w_tilde_next(-m, h, states, params, hh)
    inc = disposable_income(params.inc, params.tt, states.exo_state)

    conv = ret in [:FTOL_REACHED, :XTOL_REACHED, :SUCCESS, :LOCALLY_SOLVED]
    
    pol_full = (c=c, s=h, h=h, m=m, a=-m, w_next=w_, w_tilde=w_tilde_next_, inc=inc, occupancy=:owner, p=states.exo_state.p, ret=ret, conv=conv, count= Int(opt.numevals))
    
  end
  
  n_s = next_state(pol_full.w_tilde, pol_full.w_next, pol_full.h, hh.state)
      
  (pol=n_s, pol_full=pol_full, val=val, conv= conv)
end

function Aiyagari.get_optimum(states, 𝔼V, params, a_grid, hh::FixedHouse)
  @unpack h = states.endo_state

  inc = disposable_income(params.inc, params.tt, states.exo_state)
  
  c_max_ = c_max(h, states, params, hh)
  
  if c_max_ <= 0 || h < params.h_thres
    conv = true
    c_min = h_min = a_min = √eps()
    
    # fixed house only makes sense when adjust house is also possible,
    # so setting to -Inf shouldn't do any harm
    val = - sqrt(sqrt(prevfloat(Inf)))
    
    pol_full = (c=NaN, s=NaN, h=NaN, m=NaN, a=NaN, w_next=NaN, w_tilde=NaN, inc=inc, occupancy=:owner, p=states.exo_state.p, ret=:infeasible, conv=conv, count=0)  
    
  else

    opt = Opt(:LD_MMA, 1)
    
    lower_bounds!(opt, [eps()])
    upper_bounds!(opt, [c_max_])
    
    xtol_rel!(opt, 1e-10)
    ftol_rel!(opt, 1e-10)
    
    maxeval!(opt, 250)
      
    max_objective!(opt, (x,g) -> objective_nlopt(x, g, h, states, 𝔼V, params, hh))
      
    guesses = [0.05 * c_max_]
    (max_f, max_x, ret) = optimize(opt, guesses)

    val = max_f
    c = max_x[1]
    m = m_next(c, h, states, params, hh)
    w_ = w_next(c, h, states, params, hh)
    w_tilde_next_ = w_tilde_next(-m, h, states, params, hh)
    
    conv = ret in [:FTOL_REACHED, :XTOL_REACHED, :SUCCESS, :LOCALLY_SOLVED]
      
    pol_full = (c=c, s=h, h=h, m=m, a=-m, w_next=w_, w_tilde=w_tilde_next_, inc=inc, occupancy=:owner, p=states.exo_state.p, ret=ret, conv=conv, count= Int(opt.numevals))
  end
  
  n_s = next_state(pol_full.w_tilde, pol_full.w_next, pol_full.h, hh.state)
  
    
  (pol=n_s, pol_full=pol_full, val=val, conv= conv)
end