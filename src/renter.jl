function a_next(c, s, states, params, hh::Renter)
  @unpack endo_state, exo_state = states
  @unpack p, r = exo_state
  @unpack ϕ = params
  ϱ = ϕ * p

  w = wealth(states, params, hh)
  
  inc = disposable_income(params.inc, params.tt, states.exo_state)

  χ_ = χ(endo_state, 0, p, hh.adj)
  
  if χ_ > 0
    proceeds = w - χ_
    
    if -1 <= proceeds < 0
      tmp = (1 - 0.5 * abs(proceeds)^(1/5)) * inc
    elseif proceeds < -1
      tmp = (1 - 1/(2*abs(proceeds))) * inc
    else
      tmp = proceeds + inc
    end
  else # no adjustment costs
    
    tmp = (1+r) * w + inc
  end
  a_next = tmp - c - ϱ * s
end

function objective0(c, s, states, 𝔼V, params, hh::Renter, ::OneState)
  @unpack β, ξ = params
  
  a_next_ = a_next(c, s, states, params, hh)
  
  u(c,s, ξ=ξ) + β * 𝔼V(a_next_)  
end

function objective0(c, s, states, 𝔼V, params, hh::Renter, ::Union{WealthHouse,XAssetHouse})
  @unpack β, ξ = params
  
  a_next_ = a_next(c, s, states, params, hh)
  
  u(c,s, ξ=ξ) + β * 𝔼V(a_next_, 0)    
end

function constraint0(c, s, states, params, hh::Renter)
  @unpack r = states.exo_state
  
  a_next_ = a_next(c, s, states, params, hh)
  # - y_min / r <= a
  #(1+r) * m <= p * h * (1-δ) * θ
  - a_next_
end

function Aiyagari.get_optimum(states, 𝔼V, params, a_grid, hh::Renter)
  opt = Opt(:LD_MMA, 2)
  #opt = Opt(:LD_SLSQP, 2)
  lower_bounds!(opt, [eps(), eps()])
  upper_bounds!(opt, [+Inf, params.h_thres])
  
  xtol_rel!(opt, √eps())
  ftol_rel!(opt, eps())

  maxeval!(opt, 150)
  
  max_objective!(opt, (x,g) -> objective_nlopt(x, g, states, 𝔼V, params, hh, hh.state))
  inequality_constraint!(opt, (x,g) -> constraint_nlopt(x, g, states, params, hh), eps())
  
  inc = disposable_income(params.inc, params.tt, states.exo_state)

  @unpack p = states.exo_state
  @unpack ϕ = params
  ϱ = ϕ * p
  
  guess = (max(wealth(states, params, hh), 0) + inc)/20
  (max_f, max_x, ret) = optimize(opt, [guess, min(guess / ϱ, params.h_thres)])

  val = max_f
  c, s = max_x
  w_ = a_next(c, s, states, params, hh)

  conv = ret in [:FTOL_REACHED, :XTOL_REACHED, :SUCCESS, :LOCALLY_SOLVED]
  
  pol_full = (c=c, s=s, h=zero(s), a=w_, w_next=w_, w_tilde=w_, inc=inc, occupancy=:renter, p=states.exo_state.p, ret=ret, conv=conv, count= Int(opt.numevals))
  
  n_s = next_state(pol_full.w_tilde, pol_full.w_next, pol_full.h, hh.state)
      
  (pol=n_s, pol_full=pol_full, val=val, conv= all(conv))

end

